package main

import (
	"net/http"

	collecter "multitude-kafka-exporter/collecter"

	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	versions  string
	buildUser string = "MickeyZZC"
	buildData string
	branch    string
)

var ownRegistry = prometheus.NewRegistry()

func init() {
	ownRegistry.MustRegister(
		prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}),
		prometheus.NewGoCollector(),
	)
}

func main() {
	var (
		listenAddress       = kingpin.Flag("web.listen-address", "Address to listen on for web interface and telemetry.").Default(":59308").String()
		metricsPath         = kingpin.Flag("web.telemetry-path", "Path under which to expose metrics.").Default("/metrics").String()
		scrapePath          = kingpin.Flag("scrape-path", "Scrape path").Default("/scrape").String()
		defautlTopicWorkers = kingpin.Flag("topic.workers", "Number of topic workers").Default("100").Int()
	)
	version.Version = versions

	version.BuildUser = buildUser
	version.BuildDate = buildData
	version.Branch = branch
	kingpin.Version(version.Print("multitude-kafka-exporter"))
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()
	exporter := collecter.KafKaExporter{
		Mux:                 http.NewServeMux(),
		MetricsPath:         *metricsPath,
		ScrapePate:          *scrapePath,
		DefautlTopicWorkers: *defautlTopicWorkers,
	}

	exporter.Mux.HandleFunc("/", exporter.IndexHandler)
	exporter.Mux.HandleFunc("/health", exporter.HealthHandler)
	exporter.Mux.Handle(*metricsPath, promhttp.HandlerFor(ownRegistry, promhttp.HandlerOpts{}))
	exporter.Mux.HandleFunc(*scrapePath, exporter.ScrapeHandler)
	glog.Infoln("Listening on", *listenAddress)
	server := &http.Server{
		Addr:    *listenAddress,
		Handler: &exporter,
	}
	glog.Fatal(server.ListenAndServe())
}
