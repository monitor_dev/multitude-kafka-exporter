From golang:1.17.2-alpine3.14 as builder

RUN apk add --no-cache \
  wget \
  git

RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64 \
  && chmod +x /usr/local/bin/dumb-init

RUN mkdir -p /go/src/gitee.com/monitor_dev/multitude-kafka-exporter
WORKDIR /go/src/gitee.com/monitor_dev/multitude-kafka-exporter

# Cache dependencies
COPY go.mod .
COPY go.sum .

RUN export GOPROXY=https://goproxy.cn && GO111MODULE=on go mod download

# Build real binaries
COPY . .
RUN go build -ldflags " \
    -X main.versions=0.1.1 \
    -X main.branch=`git rev-parse HEAD` \
    -X main.buildData=`date -u '+%Y-%m-%d_%H:%M:%S'` \
    " -v -o packet/multitude-kafka-exporter cmd/custom-kafka-exporter/main.go

# Executable image
FROM alpine

COPY --from=builder /go/src/gitee.com/monitor_dev/multitude-kafka-exporter/packet/multitude-kafka-exporter /multitude-kafka-exporter
COPY --from=builder /usr/local/bin/dumb-init /usr/local/bin/dumb-init

WORKDIR /

ENTRYPOINT ["/usr/local/bin/dumb-init", "/multitude-kafka-exporter"]