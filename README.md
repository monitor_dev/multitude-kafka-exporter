# 简介：
该项目是在[danielqsj/kafka_exporter](https://github.com/danielqsj/kafka_exporter) 的基础上做了二次封装。
支持url传参方式结合服务发现实现数据采集。

```shell script
usage: main [<flags>]

Flags:
  -h, --help                   Show context-sensitive help (also try --help-long and --help-man).
      --web.listen-address=":59308"  
                               Address to listen on for web interface and telemetry.
      --web.telemetry-path="/metrics"  
                               Path under which to expose metrics.
      --scrape-path="/scrape"  Scrape path
      --topic.workers=100      Number of topic workers
      --version                Show application version.

```

`GET`传参支持如下：

| 参数名（key）    | 类型（type） | 说明                                        |
| ---------------- | ------------ | ------------------------------------------- |
| target           | String       | kafka broker地址，支持逗号分隔              |
| topic-filter     | String       | topic过滤正则                               |
| group-filter     | String       | group过滤正则                               |
| zookeeper-iplist | String       | zookeeper地址，支持逗号分隔                 |
| labels           | String       | 额外标签，支持逗号分开，每个kv标签以`=`分割 |
| kafka-version    | String       | kafka集群的版本好，默认是1.x.x              |



例子参考：

```shell
curl -X GET \
	http://localhost:59308/scrape?target=xx-1.kafka-xxx.xx.xxx.kafka.ap-southeast-1.amazonaws.com%3A9092&kafka-version=2.6.1

```

